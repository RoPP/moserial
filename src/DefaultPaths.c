/* DefaultPaths.c generated by valac 0.16.1, the Vala compiler
 * generated from DefaultPaths.vala, do not modify */

/*
 *  Copyright (C) 2009-2010 Michael J. Chudobiak.
 *
 *  This file is part of moserial.
 *
 *  moserial is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  moserial is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with moserial.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>


#define TYPE_DEFAULT_PATHS (default_paths_get_type ())
#define DEFAULT_PATHS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_DEFAULT_PATHS, DefaultPaths))
#define DEFAULT_PATHS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_DEFAULT_PATHS, DefaultPathsClass))
#define IS_DEFAULT_PATHS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_DEFAULT_PATHS))
#define IS_DEFAULT_PATHS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_DEFAULT_PATHS))
#define DEFAULT_PATHS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_DEFAULT_PATHS, DefaultPathsClass))

typedef struct _DefaultPaths DefaultPaths;
typedef struct _DefaultPathsClass DefaultPathsClass;
typedef struct _DefaultPathsPrivate DefaultPathsPrivate;
#define _g_free0(var) (var = (g_free (var), NULL))

#define TYPE_PROFILE (profile_get_type ())
#define PROFILE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PROFILE, Profile))
#define PROFILE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_PROFILE, ProfileClass))
#define IS_PROFILE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PROFILE))
#define IS_PROFILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_PROFILE))
#define PROFILE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_PROFILE, ProfileClass))

typedef struct _Profile Profile;
typedef struct _ProfileClass ProfileClass;
typedef struct _ProfilePrivate ProfilePrivate;
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))

struct _DefaultPaths {
	GObject parent_instance;
	DefaultPathsPrivate * priv;
};

struct _DefaultPathsClass {
	GObjectClass parent_class;
};

struct _DefaultPathsPrivate {
	gchar* _recordTo;
	gchar* _receiveTo;
	gchar* _sendFrom;
};

struct _Profile {
	GObject parent_instance;
	ProfilePrivate * priv;
	GKeyFile* keyFile;
};

struct _ProfileClass {
	GObjectClass parent_class;
};


static gpointer default_paths_parent_class = NULL;

GType default_paths_get_type (void) G_GNUC_CONST;
#define DEFAULT_PATHS_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), TYPE_DEFAULT_PATHS, DefaultPathsPrivate))
enum  {
	DEFAULT_PATHS_DUMMY_PROPERTY,
	DEFAULT_PATHS_RECORD_TO,
	DEFAULT_PATHS_RECEIVE_TO,
	DEFAULT_PATHS_SEND_FROM
};
DefaultPaths* default_paths_new (const gchar* RecordTo, const gchar* ReceiveTo, const gchar* SendFrom);
DefaultPaths* default_paths_construct (GType object_type, const gchar* RecordTo, const gchar* ReceiveTo, const gchar* SendFrom);
void default_paths_set_recordTo (DefaultPaths* self, const gchar* value);
void default_paths_set_receiveTo (DefaultPaths* self, const gchar* value);
void default_paths_set_sendFrom (DefaultPaths* self, const gchar* value);
GType profile_get_type (void) G_GNUC_CONST;
void default_paths_saveToProfile (DefaultPaths* self, Profile* profile);
const gchar* default_paths_get_recordTo (DefaultPaths* self);
const gchar* default_paths_get_receiveTo (DefaultPaths* self);
const gchar* default_paths_get_sendFrom (DefaultPaths* self);
DefaultPaths* default_paths_loadFromProfile (Profile* profile);
gchar* default_paths_getPath (Profile* profile, const gchar* group, const gchar* key);
gboolean mo_utils_fileExists (const gchar* path);
static void default_paths_finalize (GObject* obj);
static void _vala_default_paths_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec);
static void _vala_default_paths_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec);


DefaultPaths* default_paths_construct (GType object_type, const gchar* RecordTo, const gchar* ReceiveTo, const gchar* SendFrom) {
	DefaultPaths * self = NULL;
	const gchar* _tmp0_;
	const gchar* _tmp1_;
	const gchar* _tmp2_;
	self = (DefaultPaths*) g_object_new (object_type, NULL);
	_tmp0_ = RecordTo;
	default_paths_set_recordTo (self, _tmp0_);
	_tmp1_ = ReceiveTo;
	default_paths_set_receiveTo (self, _tmp1_);
	_tmp2_ = SendFrom;
	default_paths_set_sendFrom (self, _tmp2_);
	return self;
}


DefaultPaths* default_paths_new (const gchar* RecordTo, const gchar* ReceiveTo, const gchar* SendFrom) {
	return default_paths_construct (TYPE_DEFAULT_PATHS, RecordTo, ReceiveTo, SendFrom);
}


void default_paths_saveToProfile (DefaultPaths* self, Profile* profile) {
	const gchar* _tmp0_;
	const gchar* _tmp4_;
	const gchar* _tmp8_;
	g_return_if_fail (self != NULL);
	g_return_if_fail (profile != NULL);
	_tmp0_ = self->priv->_recordTo;
	if (_tmp0_ != NULL) {
		Profile* _tmp1_;
		GKeyFile* _tmp2_;
		const gchar* _tmp3_;
		_tmp1_ = profile;
		_tmp2_ = _tmp1_->keyFile;
		_tmp3_ = self->priv->_recordTo;
		g_key_file_set_string (_tmp2_, "paths", "last_record_path", _tmp3_);
	}
	_tmp4_ = self->priv->_receiveTo;
	if (_tmp4_ != NULL) {
		Profile* _tmp5_;
		GKeyFile* _tmp6_;
		const gchar* _tmp7_;
		_tmp5_ = profile;
		_tmp6_ = _tmp5_->keyFile;
		_tmp7_ = self->priv->_receiveTo;
		g_key_file_set_string (_tmp6_, "paths", "last_receive_path", _tmp7_);
	}
	_tmp8_ = self->priv->_sendFrom;
	if (_tmp8_ != NULL) {
		Profile* _tmp9_;
		GKeyFile* _tmp10_;
		const gchar* _tmp11_;
		_tmp9_ = profile;
		_tmp10_ = _tmp9_->keyFile;
		_tmp11_ = self->priv->_sendFrom;
		g_key_file_set_string (_tmp10_, "paths", "last_send_path", _tmp11_);
	}
}


DefaultPaths* default_paths_loadFromProfile (Profile* profile) {
	DefaultPaths* result = NULL;
	gchar* RecordTo;
	gchar* ReceiveTo;
	gchar* SendFrom;
	Profile* _tmp0_;
	gchar* _tmp1_ = NULL;
	Profile* _tmp2_;
	gchar* _tmp3_ = NULL;
	Profile* _tmp4_;
	gchar* _tmp5_ = NULL;
	const gchar* _tmp6_;
	const gchar* _tmp7_;
	const gchar* _tmp8_;
	DefaultPaths* _tmp9_;
	g_return_val_if_fail (profile != NULL, NULL);
	RecordTo = NULL;
	ReceiveTo = NULL;
	SendFrom = NULL;
	_tmp0_ = profile;
	_tmp1_ = default_paths_getPath (_tmp0_, "paths", "last_record_path");
	_g_free0 (RecordTo);
	RecordTo = _tmp1_;
	_tmp2_ = profile;
	_tmp3_ = default_paths_getPath (_tmp2_, "paths", "last_receive_path");
	_g_free0 (ReceiveTo);
	ReceiveTo = _tmp3_;
	_tmp4_ = profile;
	_tmp5_ = default_paths_getPath (_tmp4_, "paths", "last_send_path");
	_g_free0 (SendFrom);
	SendFrom = _tmp5_;
	_tmp6_ = RecordTo;
	_tmp7_ = ReceiveTo;
	_tmp8_ = SendFrom;
	_tmp9_ = default_paths_new (_tmp6_, _tmp7_, _tmp8_);
	result = _tmp9_;
	_g_free0 (SendFrom);
	_g_free0 (ReceiveTo);
	_g_free0 (RecordTo);
	return result;
}


gchar* default_paths_getPath (Profile* profile, const gchar* group, const gchar* key) {
	gchar* result = NULL;
	gchar* path;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (profile != NULL, NULL);
	g_return_val_if_fail (group != NULL, NULL);
	g_return_val_if_fail (key != NULL, NULL);
	path = NULL;
	{
		Profile* _tmp0_;
		GKeyFile* _tmp1_;
		const gchar* _tmp2_;
		const gchar* _tmp3_;
		gchar* _tmp4_ = NULL;
		gchar* _tmp5_;
		const gchar* _tmp6_;
		gboolean _tmp7_ = FALSE;
		_tmp0_ = profile;
		_tmp1_ = _tmp0_->keyFile;
		_tmp2_ = group;
		_tmp3_ = key;
		_tmp4_ = g_key_file_get_string (_tmp1_, _tmp2_, _tmp3_, &_inner_error_);
		_tmp5_ = _tmp4_;
		if (_inner_error_ != NULL) {
			if (_inner_error_->domain == G_KEY_FILE_ERROR) {
				goto __catch29_g_key_file_error;
			}
			_g_free0 (path);
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return NULL;
		}
		_g_free0 (path);
		path = _tmp5_;
		_tmp6_ = path;
		_tmp7_ = mo_utils_fileExists (_tmp6_);
		if (!_tmp7_) {
			result = NULL;
			_g_free0 (path);
			return result;
		}
	}
	goto __finally29;
	__catch29_g_key_file_error:
	{
		GError* e = NULL;
		e = _inner_error_;
		_inner_error_ = NULL;
		_g_error_free0 (e);
	}
	__finally29:
	if (_inner_error_ != NULL) {
		_g_free0 (path);
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
		g_clear_error (&_inner_error_);
		return NULL;
	}
	result = path;
	return result;
}


const gchar* default_paths_get_recordTo (DefaultPaths* self) {
	const gchar* result;
	const gchar* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_recordTo;
	result = _tmp0_;
	return result;
}


void default_paths_set_recordTo (DefaultPaths* self, const gchar* value) {
	const gchar* _tmp0_;
	gchar* _tmp1_;
	g_return_if_fail (self != NULL);
	_tmp0_ = value;
	_tmp1_ = g_strdup (_tmp0_);
	_g_free0 (self->priv->_recordTo);
	self->priv->_recordTo = _tmp1_;
	g_object_notify ((GObject *) self, "recordTo");
}


const gchar* default_paths_get_receiveTo (DefaultPaths* self) {
	const gchar* result;
	const gchar* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_receiveTo;
	result = _tmp0_;
	return result;
}


void default_paths_set_receiveTo (DefaultPaths* self, const gchar* value) {
	const gchar* _tmp0_;
	gchar* _tmp1_;
	g_return_if_fail (self != NULL);
	_tmp0_ = value;
	_tmp1_ = g_strdup (_tmp0_);
	_g_free0 (self->priv->_receiveTo);
	self->priv->_receiveTo = _tmp1_;
	g_object_notify ((GObject *) self, "receiveTo");
}


const gchar* default_paths_get_sendFrom (DefaultPaths* self) {
	const gchar* result;
	const gchar* _tmp0_;
	g_return_val_if_fail (self != NULL, NULL);
	_tmp0_ = self->priv->_sendFrom;
	result = _tmp0_;
	return result;
}


void default_paths_set_sendFrom (DefaultPaths* self, const gchar* value) {
	const gchar* _tmp0_;
	gchar* _tmp1_;
	g_return_if_fail (self != NULL);
	_tmp0_ = value;
	_tmp1_ = g_strdup (_tmp0_);
	_g_free0 (self->priv->_sendFrom);
	self->priv->_sendFrom = _tmp1_;
	g_object_notify ((GObject *) self, "sendFrom");
}


static void default_paths_class_init (DefaultPathsClass * klass) {
	default_paths_parent_class = g_type_class_peek_parent (klass);
	g_type_class_add_private (klass, sizeof (DefaultPathsPrivate));
	G_OBJECT_CLASS (klass)->get_property = _vala_default_paths_get_property;
	G_OBJECT_CLASS (klass)->set_property = _vala_default_paths_set_property;
	G_OBJECT_CLASS (klass)->finalize = default_paths_finalize;
	g_object_class_install_property (G_OBJECT_CLASS (klass), DEFAULT_PATHS_RECORD_TO, g_param_spec_string ("recordTo", "recordTo", "recordTo", NULL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (G_OBJECT_CLASS (klass), DEFAULT_PATHS_RECEIVE_TO, g_param_spec_string ("receiveTo", "receiveTo", "receiveTo", NULL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (G_OBJECT_CLASS (klass), DEFAULT_PATHS_SEND_FROM, g_param_spec_string ("sendFrom", "sendFrom", "sendFrom", NULL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
}


static void default_paths_instance_init (DefaultPaths * self) {
	self->priv = DEFAULT_PATHS_GET_PRIVATE (self);
}


static void default_paths_finalize (GObject* obj) {
	DefaultPaths * self;
	self = DEFAULT_PATHS (obj);
	_g_free0 (self->priv->_recordTo);
	_g_free0 (self->priv->_receiveTo);
	_g_free0 (self->priv->_sendFrom);
	G_OBJECT_CLASS (default_paths_parent_class)->finalize (obj);
}


GType default_paths_get_type (void) {
	static volatile gsize default_paths_type_id__volatile = 0;
	if (g_once_init_enter (&default_paths_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (DefaultPathsClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) default_paths_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (DefaultPaths), 0, (GInstanceInitFunc) default_paths_instance_init, NULL };
		GType default_paths_type_id;
		default_paths_type_id = g_type_register_static (G_TYPE_OBJECT, "DefaultPaths", &g_define_type_info, 0);
		g_once_init_leave (&default_paths_type_id__volatile, default_paths_type_id);
	}
	return default_paths_type_id__volatile;
}


static void _vala_default_paths_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec) {
	DefaultPaths * self;
	self = DEFAULT_PATHS (object);
	switch (property_id) {
		case DEFAULT_PATHS_RECORD_TO:
		g_value_set_string (value, default_paths_get_recordTo (self));
		break;
		case DEFAULT_PATHS_RECEIVE_TO:
		g_value_set_string (value, default_paths_get_receiveTo (self));
		break;
		case DEFAULT_PATHS_SEND_FROM:
		g_value_set_string (value, default_paths_get_sendFrom (self));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}


static void _vala_default_paths_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec) {
	DefaultPaths * self;
	self = DEFAULT_PATHS (object);
	switch (property_id) {
		case DEFAULT_PATHS_RECORD_TO:
		default_paths_set_recordTo (self, g_value_get_string (value));
		break;
		case DEFAULT_PATHS_RECEIVE_TO:
		default_paths_set_receiveTo (self, g_value_get_string (value));
		break;
		case DEFAULT_PATHS_SEND_FROM:
		default_paths_set_sendFrom (self, g_value_get_string (value));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}



