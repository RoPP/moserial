/* MoUtils.c generated by valac 0.16.1, the Vala compiler
 * generated from MoUtils.vala, do not modify */

/*
 *  Copyright (C) 2009-2010 Michael J. Chudobiak.
 *
 *  This file is part of moserial.
 *
 *  moserial is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  moserial is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with moserial.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <gio/gio.h>
#include <stdio.h>
#include <gtk/gtk.h>


#define TYPE_MO_UTILS (mo_utils_get_type ())
#define MO_UTILS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_MO_UTILS, MoUtils))
#define MO_UTILS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_MO_UTILS, MoUtilsClass))
#define IS_MO_UTILS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_MO_UTILS))
#define IS_MO_UTILS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_MO_UTILS))
#define MO_UTILS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_MO_UTILS, MoUtilsClass))

typedef struct _MoUtils MoUtils;
typedef struct _MoUtilsClass MoUtilsClass;
typedef struct _MoUtilsPrivate MoUtilsPrivate;
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))

#define TYPE_PROFILE (profile_get_type ())
#define PROFILE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_PROFILE, Profile))
#define PROFILE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_PROFILE, ProfileClass))
#define IS_PROFILE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_PROFILE))
#define IS_PROFILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_PROFILE))
#define PROFILE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_PROFILE, ProfileClass))

typedef struct _Profile Profile;
typedef struct _ProfileClass ProfileClass;
typedef struct _ProfilePrivate ProfilePrivate;

struct _MoUtils {
	GObject parent_instance;
	MoUtilsPrivate * priv;
};

struct _MoUtilsClass {
	GObjectClass parent_class;
};

struct _Profile {
	GObject parent_instance;
	ProfilePrivate * priv;
	GKeyFile* keyFile;
};

struct _ProfileClass {
	GObjectClass parent_class;
};


static gpointer mo_utils_parent_class = NULL;

GType mo_utils_get_type (void) G_GNUC_CONST;
enum  {
	MO_UTILS_DUMMY_PROPERTY
};
GFile* mo_utils_newFile (const gchar* path);
gboolean mo_utils_fileExists (const gchar* path);
gint64 mo_utils_fileSize (const gchar* path);
gchar* mo_utils_getParentFolder (const gchar* path);
GType profile_get_type (void) G_GNUC_CONST;
gchar* mo_utils_getKeyString (Profile* profile, const gchar* group, const gchar* key);
gint mo_utils_getKeyInteger (Profile* profile, const gchar* group, const gchar* key, gint default_val);
gboolean mo_utils_getKeyBoolean (Profile* profile, const gchar* group, const gchar* key, gboolean default_val);
gchar* mo_utils_getLastMessage (const gchar* messages);
gchar* input_parser_statusReplace (const gchar* oldString);
void mo_utils_populateComboBox (GtkComboBox* Combo, gchar** val_array, int val_array_length1);
MoUtils* mo_utils_new (void);
MoUtils* mo_utils_construct (GType object_type);
static void _vala_array_destroy (gpointer array, gint array_length, GDestroyNotify destroy_func);
static void _vala_array_free (gpointer array, gint array_length, GDestroyNotify destroy_func);
static gint _vala_array_length (gpointer array);


static gboolean string_contains (const gchar* self, const gchar* needle) {
	gboolean result = FALSE;
	const gchar* _tmp0_;
	gchar* _tmp1_ = NULL;
	g_return_val_if_fail (self != NULL, FALSE);
	g_return_val_if_fail (needle != NULL, FALSE);
	_tmp0_ = needle;
	_tmp1_ = strstr ((gchar*) self, (gchar*) _tmp0_);
	result = _tmp1_ != NULL;
	return result;
}


GFile* mo_utils_newFile (const gchar* path) {
	GFile* result = NULL;
	gchar* uri = NULL;
	const gchar* _tmp0_;
	gboolean _tmp1_ = FALSE;
	const gchar* _tmp6_;
	GFile* _tmp7_ = NULL;
	g_return_val_if_fail (path != NULL, NULL);
	_tmp0_ = path;
	_tmp1_ = string_contains (_tmp0_, "://");
	if (_tmp1_) {
		const gchar* _tmp2_;
		gchar* _tmp3_;
		_tmp2_ = path;
		_tmp3_ = g_strdup (_tmp2_);
		_g_free0 (uri);
		uri = _tmp3_;
	} else {
		const gchar* _tmp4_;
		gchar* _tmp5_ = NULL;
		_tmp4_ = path;
		_tmp5_ = g_strdup_printf ("file://%s", _tmp4_);
		_g_free0 (uri);
		uri = _tmp5_;
	}
	_tmp6_ = uri;
	_tmp7_ = g_file_new_for_uri (_tmp6_);
	result = _tmp7_;
	_g_free0 (uri);
	return result;
}


gboolean mo_utils_fileExists (const gchar* path) {
	gboolean result = FALSE;
	const gchar* _tmp0_;
	GFile* _tmp1_ = NULL;
	GFile* file;
	gboolean _tmp2_ = FALSE;
	g_return_val_if_fail (path != NULL, FALSE);
	_tmp0_ = path;
	_tmp1_ = mo_utils_newFile (_tmp0_);
	file = _tmp1_;
	_tmp2_ = g_file_query_exists (file, NULL);
	result = _tmp2_;
	_g_object_unref0 (file);
	return result;
}


gint64 mo_utils_fileSize (const gchar* path) {
	gint64 result = 0LL;
	const gchar* _tmp0_;
	GFile* _tmp1_ = NULL;
	GFile* file;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (path != NULL, 0LL);
	_tmp0_ = path;
	_tmp1_ = mo_utils_newFile (_tmp0_);
	file = _tmp1_;
	{
		GFileInfo* _tmp2_ = NULL;
		GFileInfo* info;
		gint64 _tmp3_ = 0LL;
		_tmp2_ = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_SIZE, 0, NULL, &_inner_error_);
		info = _tmp2_;
		if (_inner_error_ != NULL) {
			goto __catch30_g_error;
		}
		_tmp3_ = g_file_info_get_size (info);
		result = _tmp3_;
		_g_object_unref0 (info);
		_g_object_unref0 (file);
		return result;
	}
	goto __finally30;
	__catch30_g_error:
	{
		GError* e = NULL;
		GError* _tmp4_;
		const gchar* _tmp5_;
		e = _inner_error_;
		_inner_error_ = NULL;
		_tmp4_ = e;
		_tmp5_ = _tmp4_->message;
		g_warning ("MoUtils.vala:49: %s", _tmp5_);
		_g_error_free0 (e);
	}
	__finally30:
	if (_inner_error_ != NULL) {
		_g_object_unref0 (file);
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
		g_clear_error (&_inner_error_);
		return 0LL;
	}
	result = (gint64) 0;
	_g_object_unref0 (file);
	return result;
}


gchar* mo_utils_getParentFolder (const gchar* path) {
	gchar* result = NULL;
	const gchar* _tmp0_;
	GFile* _tmp1_ = NULL;
	GFile* file;
	GFile* _tmp2_ = NULL;
	GFile* parent;
	gchar* _tmp3_ = NULL;
	g_return_val_if_fail (path != NULL, NULL);
	_tmp0_ = path;
	_tmp1_ = mo_utils_newFile (_tmp0_);
	file = _tmp1_;
	_tmp2_ = g_file_get_parent (file);
	parent = _tmp2_;
	_tmp3_ = g_file_get_parse_name (parent);
	result = _tmp3_;
	_g_object_unref0 (parent);
	_g_object_unref0 (file);
	return result;
}


gchar* mo_utils_getKeyString (Profile* profile, const gchar* group, const gchar* key) {
	gchar* result = NULL;
	gchar* _result_;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (profile != NULL, NULL);
	g_return_val_if_fail (group != NULL, NULL);
	g_return_val_if_fail (key != NULL, NULL);
	_result_ = NULL;
	{
		Profile* _tmp0_;
		GKeyFile* _tmp1_;
		const gchar* _tmp2_;
		const gchar* _tmp3_;
		gchar* _tmp4_ = NULL;
		gchar* _tmp5_;
		_tmp0_ = profile;
		_tmp1_ = _tmp0_->keyFile;
		_tmp2_ = group;
		_tmp3_ = key;
		_tmp4_ = g_key_file_get_string (_tmp1_, _tmp2_, _tmp3_, &_inner_error_);
		_tmp5_ = _tmp4_;
		if (_inner_error_ != NULL) {
			if (_inner_error_->domain == G_KEY_FILE_ERROR) {
				goto __catch31_g_key_file_error;
			}
			_g_free0 (_result_);
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return NULL;
		}
		_g_free0 (_result_);
		_result_ = _tmp5_;
	}
	goto __finally31;
	__catch31_g_key_file_error:
	{
		GError* e = NULL;
		FILE* _tmp6_;
		GError* _tmp7_;
		const gchar* _tmp8_;
		e = _inner_error_;
		_inner_error_ = NULL;
		_tmp6_ = stdout;
		_tmp7_ = e;
		_tmp8_ = _tmp7_->message;
		fprintf (_tmp6_, "%s\n", _tmp8_);
		_g_error_free0 (e);
	}
	__finally31:
	if (_inner_error_ != NULL) {
		_g_free0 (_result_);
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
		g_clear_error (&_inner_error_);
		return NULL;
	}
	result = _result_;
	return result;
}


gint mo_utils_getKeyInteger (Profile* profile, const gchar* group, const gchar* key, gint default_val) {
	gint result = 0;
	gint _tmp0_;
	gint _result_;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (profile != NULL, 0);
	g_return_val_if_fail (group != NULL, 0);
	g_return_val_if_fail (key != NULL, 0);
	_tmp0_ = default_val;
	_result_ = _tmp0_;
	{
		Profile* _tmp1_;
		GKeyFile* _tmp2_;
		const gchar* _tmp3_;
		const gchar* _tmp4_;
		gint _tmp5_ = 0;
		gint _tmp6_;
		_tmp1_ = profile;
		_tmp2_ = _tmp1_->keyFile;
		_tmp3_ = group;
		_tmp4_ = key;
		_tmp5_ = g_key_file_get_integer (_tmp2_, _tmp3_, _tmp4_, &_inner_error_);
		_tmp6_ = _tmp5_;
		if (_inner_error_ != NULL) {
			if (_inner_error_->domain == G_KEY_FILE_ERROR) {
				goto __catch32_g_key_file_error;
			}
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return 0;
		}
		_result_ = _tmp6_;
	}
	goto __finally32;
	__catch32_g_key_file_error:
	{
		GError* e = NULL;
		FILE* _tmp7_;
		GError* _tmp8_;
		const gchar* _tmp9_;
		e = _inner_error_;
		_inner_error_ = NULL;
		_tmp7_ = stdout;
		_tmp8_ = e;
		_tmp9_ = _tmp8_->message;
		fprintf (_tmp7_, "%s\n", _tmp9_);
		_g_error_free0 (e);
	}
	__finally32:
	if (_inner_error_ != NULL) {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
		g_clear_error (&_inner_error_);
		return 0;
	}
	result = _result_;
	return result;
}


gboolean mo_utils_getKeyBoolean (Profile* profile, const gchar* group, const gchar* key, gboolean default_val) {
	gboolean result = FALSE;
	gboolean _tmp0_;
	gboolean _result_;
	GError * _inner_error_ = NULL;
	g_return_val_if_fail (profile != NULL, FALSE);
	g_return_val_if_fail (group != NULL, FALSE);
	g_return_val_if_fail (key != NULL, FALSE);
	_tmp0_ = default_val;
	_result_ = _tmp0_;
	{
		Profile* _tmp1_;
		GKeyFile* _tmp2_;
		const gchar* _tmp3_;
		const gchar* _tmp4_;
		gboolean _tmp5_ = FALSE;
		gboolean _tmp6_;
		_tmp1_ = profile;
		_tmp2_ = _tmp1_->keyFile;
		_tmp3_ = group;
		_tmp4_ = key;
		_tmp5_ = g_key_file_get_boolean (_tmp2_, _tmp3_, _tmp4_, &_inner_error_);
		_tmp6_ = _tmp5_;
		if (_inner_error_ != NULL) {
			if (_inner_error_->domain == G_KEY_FILE_ERROR) {
				goto __catch33_g_key_file_error;
			}
			g_critical ("file %s: line %d: unexpected error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
			g_clear_error (&_inner_error_);
			return FALSE;
		}
		_result_ = _tmp6_;
	}
	goto __finally33;
	__catch33_g_key_file_error:
	{
		GError* e = NULL;
		FILE* _tmp7_;
		GError* _tmp8_;
		const gchar* _tmp9_;
		e = _inner_error_;
		_inner_error_ = NULL;
		_tmp7_ = stdout;
		_tmp8_ = e;
		_tmp9_ = _tmp8_->message;
		fprintf (_tmp7_, "%s\n", _tmp9_);
		_g_error_free0 (e);
	}
	__finally33:
	if (_inner_error_ != NULL) {
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
		g_clear_error (&_inner_error_);
		return FALSE;
	}
	result = _result_;
	return result;
}


gchar* mo_utils_getLastMessage (const gchar* messages) {
	gchar* result = NULL;
	gchar* message;
	const gchar* _tmp0_;
	gchar* _tmp1_ = NULL;
	gchar* escaped;
	const gchar* _tmp2_;
	gchar* _tmp3_ = NULL;
	gchar** splitMessages = NULL;
	gint splitMessages_length1 = 0;
	gint _splitMessages_size_ = 0;
	const gchar* _tmp4_;
	gchar** _tmp5_;
	gchar** _tmp6_ = NULL;
	message = NULL;
	_tmp0_ = messages;
	_tmp1_ = g_strescape (_tmp0_, "");
	escaped = _tmp1_;
	_tmp2_ = escaped;
	_tmp3_ = input_parser_statusReplace (_tmp2_);
	_g_free0 (escaped);
	escaped = _tmp3_;
	_tmp4_ = escaped;
	_tmp6_ = _tmp5_ = g_strsplit (_tmp4_, "\\n", 20);
	splitMessages = (_vala_array_free (splitMessages, splitMessages_length1, (GDestroyNotify) g_free), NULL);
	splitMessages = _tmp6_;
	splitMessages_length1 = _vala_array_length (_tmp5_);
	_splitMessages_size_ = splitMessages_length1;
	{
		gint x;
		x = 0;
		{
			gboolean _tmp7_;
			_tmp7_ = TRUE;
			while (TRUE) {
				gboolean _tmp8_;
				gint _tmp10_;
				gchar** _tmp11_;
				gint _tmp11__length1;
				guint _tmp12_ = 0U;
				gchar** _tmp13_;
				gint _tmp13__length1;
				gint _tmp14_;
				const gchar* _tmp15_;
				gint _tmp16_;
				gint _tmp17_;
				_tmp8_ = _tmp7_;
				if (!_tmp8_) {
					gint _tmp9_;
					_tmp9_ = x;
					x = _tmp9_ + 1;
				}
				_tmp7_ = FALSE;
				_tmp10_ = x;
				_tmp11_ = splitMessages;
				_tmp11__length1 = splitMessages_length1;
				_tmp12_ = g_strv_length (_tmp11_);
				if (!(((guint) _tmp10_) < _tmp12_)) {
					break;
				}
				_tmp13_ = splitMessages;
				_tmp13__length1 = splitMessages_length1;
				_tmp14_ = x;
				_tmp15_ = _tmp13_[_tmp14_];
				_tmp16_ = strlen (_tmp15_);
				_tmp17_ = _tmp16_;
				if (_tmp17_ > 5) {
					gchar** _tmp18_;
					gint _tmp18__length1;
					gint _tmp19_;
					const gchar* _tmp20_;
					gchar* _tmp21_;
					_tmp18_ = splitMessages;
					_tmp18__length1 = splitMessages_length1;
					_tmp19_ = x;
					_tmp20_ = _tmp18_[_tmp19_];
					_tmp21_ = g_strdup (_tmp20_);
					_g_free0 (message);
					message = _tmp21_;
				}
			}
		}
	}
	result = message;
	splitMessages = (_vala_array_free (splitMessages, splitMessages_length1, (GDestroyNotify) g_free), NULL);
	_g_free0 (escaped);
	return result;
}


void mo_utils_populateComboBox (GtkComboBox* Combo, gchar** val_array, int val_array_length1) {
	GtkListStore* _tmp0_;
	GtkListStore* Model;
	gchar** _tmp1_;
	gint _tmp1__length1;
	GtkComboBox* _tmp8_;
	GtkListStore* _tmp9_;
	GtkCellRendererText* _tmp10_;
	GtkCellRenderer* _tmp11_;
	GtkCellRenderer* Cell;
	GtkComboBox* _tmp12_;
	GtkComboBox* _tmp13_;
	g_return_if_fail (Combo != NULL);
	_tmp0_ = gtk_list_store_new (1, G_TYPE_STRING);
	Model = _tmp0_;
	_tmp1_ = val_array;
	_tmp1__length1 = val_array_length1;
	{
		gchar** val_item_collection = NULL;
		gint val_item_collection_length1 = 0;
		gint _val_item_collection_size_ = 0;
		gint val_item_it = 0;
		val_item_collection = _tmp1_;
		val_item_collection_length1 = _tmp1__length1;
		for (val_item_it = 0; val_item_it < _tmp1__length1; val_item_it = val_item_it + 1) {
			gchar* _tmp2_;
			gchar* val_item = NULL;
			_tmp2_ = g_strdup (val_item_collection[val_item_it]);
			val_item = _tmp2_;
			{
				GtkTreeIter iter = {0};
				GtkListStore* _tmp3_;
				GtkTreeIter _tmp4_ = {0};
				GtkListStore* _tmp5_;
				GtkTreeIter _tmp6_;
				const gchar* _tmp7_;
				_tmp3_ = Model;
				gtk_list_store_append (_tmp3_, &_tmp4_);
				iter = _tmp4_;
				_tmp5_ = Model;
				_tmp6_ = iter;
				_tmp7_ = val_item;
				gtk_list_store_set (_tmp5_, &_tmp6_, 0, _tmp7_, -1);
				_g_free0 (val_item);
			}
		}
	}
	_tmp8_ = Combo;
	_tmp9_ = Model;
	gtk_combo_box_set_model (_tmp8_, (GtkTreeModel*) _tmp9_);
	_tmp10_ = (GtkCellRendererText*) gtk_cell_renderer_text_new ();
	_tmp11_ = (GtkCellRenderer*) g_object_ref_sink (_tmp10_);
	Cell = _tmp11_;
	_tmp12_ = Combo;
	gtk_cell_layout_pack_start ((GtkCellLayout*) _tmp12_, Cell, TRUE);
	_tmp13_ = Combo;
	gtk_cell_layout_set_attributes ((GtkCellLayout*) _tmp13_, Cell, "text", 0, NULL);
	_g_object_unref0 (Cell);
	_g_object_unref0 (Model);
}


MoUtils* mo_utils_construct (GType object_type) {
	MoUtils * self = NULL;
	self = (MoUtils*) g_object_new (object_type, NULL);
	return self;
}


MoUtils* mo_utils_new (void) {
	return mo_utils_construct (TYPE_MO_UTILS);
}


static void mo_utils_class_init (MoUtilsClass * klass) {
	mo_utils_parent_class = g_type_class_peek_parent (klass);
}


static void mo_utils_instance_init (MoUtils * self) {
}


GType mo_utils_get_type (void) {
	static volatile gsize mo_utils_type_id__volatile = 0;
	if (g_once_init_enter (&mo_utils_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (MoUtilsClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) mo_utils_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (MoUtils), 0, (GInstanceInitFunc) mo_utils_instance_init, NULL };
		GType mo_utils_type_id;
		mo_utils_type_id = g_type_register_static (G_TYPE_OBJECT, "MoUtils", &g_define_type_info, 0);
		g_once_init_leave (&mo_utils_type_id__volatile, mo_utils_type_id);
	}
	return mo_utils_type_id__volatile;
}


static void _vala_array_destroy (gpointer array, gint array_length, GDestroyNotify destroy_func) {
	if ((array != NULL) && (destroy_func != NULL)) {
		int i;
		for (i = 0; i < array_length; i = i + 1) {
			if (((gpointer*) array)[i] != NULL) {
				destroy_func (((gpointer*) array)[i]);
			}
		}
	}
}


static void _vala_array_free (gpointer array, gint array_length, GDestroyNotify destroy_func) {
	_vala_array_destroy (array, array_length, destroy_func);
	g_free (array);
}


static gint _vala_array_length (gpointer array) {
	int length;
	length = 0;
	if (array) {
		while (((gpointer*) array)[length]) {
			length++;
		}
	}
	return length;
}



