<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "3.0.0">
<!ENTITY manualversion "3.0.0">
<!ENTITY date "April 2009">
<!ENTITY appname "moserial">
<!ENTITY app "<application>moserial</application>">
<!-- replace version above with actual application version number --><!--  Template Version: 1.0.1  (do not remove this line) -->]>
<!-- =============Document Header ============================= -->
<article id="index" lang="fr"> <!-- please do not change the id -->

  <articleinfo>
    <title>Manuel de <application>moserial</application> v3.0.0</title>
    <copyright>
      <year>2009</year>
      <holder>Michael J. Chudobiak</holder>
    </copyright><copyright><year>2011</year><holder>Claude Paroz (claude@2xlibre.net)</holder></copyright>

    <!-- translators: uncomment this:

    <copyright>
     <year>2000</year>
     <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
    </copyright>

    -->

    <!-- An address can be added to the publisher information.  If a role is
     not specified, the publisher/author is the same for all versions of the
     document.  -->
    <publisher>
      <publishername>Projet de documentation GNOME</publishername>
    </publisher>

       <legalnotice id="legalnotice">
         <para>Permission vous est donnée de copier, distribuer et/ou modifier ce document selon les termes de la Licence GNU Free Documentation License, Version 1.1 ou ultérieure publiée par la Free Software Foundation sans section inaltérable, sans texte de première page de couverture ni texte de dernière page de couverture. Vous trouverez un exemplaire de cette licence en suivant ce <ulink type="help" url="ghelp:fdl">lien</ulink> ou dans le fichier COPYING-DOCS fourni avec le présent manuel.</para>
          <para>Ce manuel fait partie de la collection de manuels GNOME distribués selon les termes de la licence de documentation libre GNU. Si vous souhaitez distribuer ce manuel indépendamment de la collection, vous devez joindre un exemplaire de la licence au document, comme indiqué dans la section 6 de celle-ci.</para>
 
         <para>La plupart des noms utilisés par les entreprises pour distinguer leurs produits et services sont des marques déposées. Lorsque ces noms apparaissent dans la documentation GNOME et que les membres du projet de Documentation GNOME sont informés de l'existence de ces marques déposées, soit ces noms entiers, soit leur première lettre est en majuscule.</para>
 
         <para>LE PRÉSENT DOCUMENT ET SES VERSIONS MODIFIÉES SONT FOURNIS SELON LES TERMES DE LA LICENCE DE DOCUMENTATION LIBRE GNU SACHANT QUE : <orderedlist>
                 <listitem>
                   <para>LE PRÉSENT DOCUMENT EST FOURNI « TEL QUEL », SANS AUCUNE GARANTIE, EXPRESSE OU IMPLICITE, Y COMPRIS, ET SANS LIMITATION, LES GARANTIES DE BONNE QUALITÉ MARCHANDE OU D'APTITUDE À UN EMPLOI PARTICULIER OU AUTORISÉ DU DOCUMENT OU DE SA VERSION MODIFIÉE. L'UTILISATEUR ASSUME TOUT RISQUE RELATIF À LA QUALITÉ, À LA PERTINENCE ET À LA PERFORMANCE DU DOCUMENT OU DE SA VERSION DE MISE À JOUR. SI LE DOCUMENT OU SA VERSION MODIFIÉE S'AVÉRAIT DÉFECTUEUSE, L'UTILISATEUR (ET NON LE RÉDACTEUR INITIAL, L'AUTEUR, NI TOUT AUTRE PARTICIPANT) ENDOSSERA LES COÛTS DE TOUTE INTERVENTION, RÉPARATION OU CORRECTION NÉCESSAIRE. CETTE DÉNÉGATION DE RESPONSABILITÉ CONSTITUE UNE PARTIE ESSENTIELLE DE CETTE LICENCE. AUCUNE UTILISATION DE CE DOCUMENT OU DE SA VERSION MODIFIÉE N'EST AUTORISÉE AUX TERMES DU PRÉSENT ACCORD, EXCEPTÉ SOUS CETTE DÉNÉGATION DE RESPONSABILITÉ ; ET</para>
                 </listitem>
                 <listitem>
                   <para>EN AUCUNE CIRCONSTANCE ET SOUS AUCUNE INTERPRÉTATION DE LA LOI, QU'IL S'AGISSE DE RESPONSABILITÉ CIVILE (Y COMPRIS LA NÉGLIGENCE), CONTRACTUELLE OU AUTRE, L'AUTEUR, LE RÉDACTEUR INITIAL, TOUT PARTICIPANT ET TOUT DISTRIBUTEUR DE CE DOCUMENT OU DE SA VERSION DE MISE À JOUR AINSI QUE TOUT FOURNISSEUR DE QUELQUE PARTIE QUE CE SOIT NE POURRONT ÊTRE TENUS RESPONSABLES À L'ÉGARD DE QUICONQUE POUR TOUT DOMMAGE DIRECT, INDIRECT, PARTICULIER OU ACCIDENTEL DE TOUT TYPE Y COMPRIS, SANS LIMITATION, LES DOMMAGES LIÉS À LA PERTE DE CLIENTÈLE, AUX ARRÊTS DE TRAVAIL, AUX DÉFAILLANCES ET AUX DYSFONCTIONNEMENTS INFORMATIQUES OU TOUT AUTRE DOMMAGE OU PERTE LIÉE À L'UTILISATION DU PRÉSENT DOCUMENT ET DE SES VERSIONS DE MISE À JOUR, ET CE MÊME SI CES PARTIES ONT ÉTÉ INFORMÉES DE LA POSSIBILITÉ DE TELS DOMMAGES.</para>
                 </listitem>
           </orderedlist></para>
   </legalnotice>
 

    <authorgroup>
      <author role="maintainer">
        <firstname>Michael</firstname>
	<surname>Chudobiak</surname>
	<affiliation>
	  <address> <email>mjc@svn.gnome.org</email> </address>
        </affiliation>
      </author>
    </authorgroup>
    <!-- do not put authorname in the header except in copyright - use
         section "authors" below -->
    <revhistory>
      <revision>
	<revnumber>3.0.0</revnumber>
	<date>Avril 2009</date>
	<revdescription>
	  <para role="author">Michael J. Chudobiak</para>
	  <para role="publisher">Projet de documentation GNOME</para>
	</revdescription>
      </revision>
    </revhistory>


    <!-- this is version of manual, not application -->
    <releaseinfo>Ce manuel documente la version 3.0.0 de moserial</releaseinfo>
    <abstract role="description"><para><application>moserial</application> est un terminal série pour le bureau GNOME, optimisé pour la journalisation et la capture de fichiers.</para></abstract>
  </articleinfo>

  <indexterm>
    <primary>moserial</primary>
  </indexterm>
  <indexterm>
    <primary>série</primary>
  </indexterm>
  <indexterm>
    <secondary>terminal</secondary>
  </indexterm>

  <!-- ============= Document Body ============================= -->

  <!-- ============= Introduction ============================== -->

  <sect1 id="intro">
    <title>Introduction</title>
    <para><application>moserial</application> est un terminal série pour le bureau GNOME, optimisé pour la journalisation et la capture de fichiers. Il est avant tout destiné aux utilisateurs techniques devant communiquer avec des systèmes embarqués, des consoles et des matériels de test électroniques.</para>
    <para>Malgré l'orientation technique de l'audience visée, <application>moserial</application> est conçu pour être facile à utiliser. Un soin particulier a été apporté à ce que les opérations soient simples à exécuter, en minimisant le nombre de clics nécessaires. Le nombre d'options de configuration reste faible.</para>
    <para>Pour accroître l'efficacité, <application>moserial</application> gère aussi des fichiers de profil pour conserver les configurations les plus utilisées.</para>
  </sect1>

  <sect1 id="ports">
    <title>Droits d'accès aux ports systèmes</title>
    <para>Avant d'utiliser <application>moserial</application> avec un port série, vous devez vous assurer que vous possédez bien les droits d'accès à ce port pour lire et écrire.</para>
    <para>Sur d'autres systèmes, vous aurez peut-être à lancer des commandes semblables à celles-ci : « sudo chmod o+rw /dev/ttyS* » ou « sudo chmod o+rw /dev/ttyUSB* », pour obtenir les droits d'accès aux ports série en lecture et en écriture (pour tous les utilisateurs). Malheureusement, ces réglages ne sont pas toujours conservés lors d'un redémarrage.</para>
    <para>Sinon, vous pouvez voir à quel groupe appartiennent les périphériques et ajouter votre compte comme membre de ce groupe. Avec Fedora 15, par exemple, il faut vous ajouter au groupe « dialout ».</para>
  </sect1>

  <sect1 id="basic-usage">
    <title>Utilisation de base</title>
    <sect2 id="main-toolbar">
      <title>Barre d'outils principale</title>
      <para>La plupart des fonctions de <application>moserial</application> sont accessibles par la barre d'outils principale, affichée ci-dessous :</para>
      <para>
        <graphic format="PNG" fileref="figures/main-toolbar.png"/>
      </para>
    </sect2>
    <sect2 id="port-setup">
      <title>Paramétrage de port</title>
      <para>La première étape de l'utilisation de <application>moserial</application> est la configuration des paramètres du port série. Pour cela, cliquez sur <guibutton>Paramétrage de port</guibutton>. Dans la boîte de dialogue Paramétrage de port, vous définirez le port à utiliser, s'il doit être ouvert en lecture, en écriture, ou les deux, la vitesse de transmission en bauds, sa parité, le nombre de bits de données et d'arrêt, et la méthode de contrôle de flux. Les paramètres définis dans cette fenêtre écraseront les paramètres système définis par défaut pour ce port.</para>
      <para>La boîte de dialogue Paramétrage de port de <application>moserial</application> n'énumère que les ports /dev/ttySnn, /dev/ttyUSBnn et /dev/rfcommnn effectivement présents sur le système, où les valeurs « nn » sont comprises entre 0 et 31.</para>
      <para>Un écho local peut être activé dans cette boîte de dialogue. Dans ce cas, toutes les données que <application>moserial</application> reçoit seront répétées dans le périphérique d'envoi. Cette option devrait normalement être désactivée, car elle peut être cause de comportements anormaux, surtout si elle est également activée dans le périphérique distant (risque d'écho en boucle infinie).</para>
      <para>
        <graphic format="PNG" fileref="figures/port-setup.png"/>
      </para>
    </sect2>
    <sect2 id="connect">
      <title>Ouverture de la connexion</title>
      <para>L'étape suivante pour communiquer avec le périphérique distant est de cliquer sur <guibutton>Connecter</guibutton>, ce qui ouvre le port. Si vous n'avez pas les droits d'accès à ce port, une erreur sera émise.</para>
      <para>
	<graphic format="PNG" fileref="figures/connect.png"/>
      </para>
      <para>Cette étape est facultative. Si vous l'omettez, <application>moserial</application> tentera de se connecter automatiquement quand ce sera nécessaire.</para>
      <para>Une fois connecté, le bouton <guibutton>Connecter</guibutton> se change en bouton <guibutton>Déconnecter</guibutton>. Le fait de cliquer sur <guibutton>Déconnecter</guibutton> ferme le port et réinitialise les compteurs TX et RX.</para>
      <para>
         <graphic format="PNG" fileref="figures/disconnect.png"/>
      </para>
    </sect2>
    <sect2 id="basic-io">
      <title>Entrée et sortie de base</title>
      <para>Une fois le port ouvert, tout texte reçu depuis le périphérique distant s'affiche dans la zone de texte <guilabel>ASCII reçu</guilabel>. Si <application>moserial</application> détecte un grand nombre de caractères non imprimables, il passera automatiquement en visualisation <guilabel>HEX reçu</guilabel>.</para>
      <para>
        <graphic format="PNG" fileref="figures/received-tabs.png"/>
      </para>
      <para>Pour basculer entre les visualisations HEX et ASCII, cliquez simplement sur les onglets correspondants.</para>
      <para>Pour envoyer des données au périphérique distant, tapez le texte dans la boîte uni-ligne <guilabel>Sortant</guilabel> et cliquez sur le bouton <guibutton>Envoyer</guibutton>. Votre ligne de texte se terminera par la séquence sélectionnée dans la liste déroulante tout à droite du bouton <guibutton>Envoyer</guibutton>. Si vous n'êtes pas sûr de cette séquence de terminaison, essayez d'abord « CR+LF end ». Le bouton <guibutton>ASCII/HEX</guibutton> doit être sur ASCII pour envoyer du texte.</para>
      <para>
        <graphic format="PNG" fileref="figures/outgoing.png"/>
      </para>
      <para>Si le bouton <guibutton>ASCII/HEX</guibutton> est sur HEX, ne saisissez pas de texte dans la zone <guilabel>Sortant</guilabel>. À la place, saisissez des caractères hexadécimaux sous la forme « 1234ABCD.... ».</para>
      <para>Après transmission des données, un enregistrement de ces données se trouvera dans les zones de texte <guilabel>ASCII envoyé</guilabel> et <guilabel>HEX envoyé</guilabel>. Ces zones de texte ne sont pas modifiables.</para>
      <para>
        <graphic format="PNG" fileref="figures/sent-tabs.png"/>
      </para>
      <para>Le périphérique distant peut faire écho aux données envoyées. Si <application>moserial</application> détecte un texte répété dans la zone de texte <guilabel>ASCII reçu</guilabel>, il mettra le texte répété en surbrillance (en utilisant la couleur de surbrillance définie dans la boîte de dialogue des préférences).</para>
      <para>La barre d'état, en bas de la fenêtre principale, affiche l'état du port ainsi que le compte des octets envoyés (TX) et reçus (RX). Si certaines des données ne peuvent être affichées, le compte de ces données est néanmoins présenté.</para>
      <para>
        <graphic format="PNG" fileref="figures/statusbar.png"/>
      </para>
    </sect2>
  </sect1>
  <sect1 id="recording">
    <title>Enregistrement des entrées et des sorties</title>
    <para><application>moserial</application> peut enregistrer dans un fichier les données envoyées et reçues. Cette fonction est également utile pour recevoir et enregistrer directement des données binaires déversées depuis un périphérique distant. C'est le cas de certains oscilloscopes qui dirigent des captures d'écran vers un port série, sans recourir à un protocole de correction d'erreurs (tel le Tektronix 11801).</para>
    <para>
      <graphic format="PNG" fileref="figures/record.png"/>
    </para>
    <para>Quand vous êtes prêt à enregistrer vers un fichier, cliquez simplement sur <guibutton>Enregistrer</guibutton>, sélectionnez les données que vous voulez enregistrer (entrantes et/ou sortantes) dans la boîte de sélection du <guilabel>Flux à enregistrer</guilabel> et saisissez le nom de fichier.</para>
    <para>
      <graphic format="PNG" fileref="figures/record-dialog.png"/>
    </para>
    <para>Quand l'enregistrement commence, le bouton <guibutton>Enregistrer</guibutton> devient <guibutton>Arrêter l'enregistrement</guibutton>. Vous devez cliquer sur ce bouton quand vous souhaitez arrêter l'enregistrement ou quand vous avez reçu la totalité du fichier binaire que vous souhaitiez télécharger.</para>
    <para>
      <graphic format="PNG" fileref="figures/stop-recording.png"/>
    </para>
    <para><application>moserial</application> peut éventuellement lancer le fichier enregistré, avec l'application par défaut pour son type de fichier, quand l'enregistrement est fini. Il peut aussi interrompre l'enregistrement après une période d'inactivité. Utilisez les <link linkend="preferences">Préférences</link> pour activer ou désactiver ces options.</para>
    <para>Notez que si vous souhaitez recevoir des fichiers en utilisant un protocole de correction d'erreurs (xmodem, ymodem ou zmodem), vous devez utilisez la fonction <link linkend="receiving">Recevoir un fichier</link>. Contrairement à la fonction <guibutton>Enregistrer</guibutton>, la réception de fichier peut automatiquement détecter la fin d'un téléchargement de fichier.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-file.png"/>
    </para>
  </sect1>
  <sect1 id="sending">
    <title>Envoi de fichiers</title>
    <para><application>moserial</application> peut transmettre des fichiers dans un flux de données binaires direct, mais il peut aussi les transmettre en passant par les protocoles de correction d'erreurs xmodem, ymodem et zmodem. Pendant la transmission de fichier, les données envoyées ne sont pas reliées aux zones de texte <guilabel>ASCII envoyé</guilabel> ou <guilabel>HEX envoyé</guilabel>.</para>
    <para>
      <graphic format="PNG" fileref="figures/send-file.png"/>
    </para>
    <para>Quand vous êtes prêt à envoyer un fichier, cliquez simplement sur <guibutton>Envoyer un fichier</guibutton>. Vous devrez aussi sélectionner le protocole approprié en bas de la fenêtre.</para>
    <para>
      <graphic format="PNG" fileref="figures/send-file-dialog.png"/>
    </para>
    <para>Pendant l'enregistrement, une barre de progression affiche l'état de transmission du fichier.</para>
    <para>
      <graphic format="PNG" fileref="figures/send-progress.png"/>
    </para>
    <para>moserial n'applique pas les protocoles xmodem, ymodem ou zmodem directement. Il dépend des utilitaires standards rz et sz pour envoyer et recevoir des données. Ces utilitaires font partie du paquet lrzsz, qui doit être installé sur votre système.</para>
  </sect1>

  <sect1 id="receiving">
    <title>Réception de fichiers</title>
    <para><application>moserial</application> peut recevoir des fichiers dans un flux direct de données binaires, mais il peut aussi les recevoir en passant par les protocoles de correction d'erreurs xmodem, ymodem et zmodem. Pendant la transmission de fichier, les données reçues ne sont pas reliées aux zones de texte <guilabel>ASCII reçu</guilabel> ou <guilabel>HEX reçu</guilabel>.</para>
    <para>Pour recevoir des flux de données binaires directs, utilisez la fonction <link linkend="recording">Enregistrer</link> plutôt que la fonction <guibutton>Recevoir un fichier</guibutton>.</para>
    <para>Pour recevoir des fichiers envoyés avec les protocoles de correction d'erreurs xmodem, ymodem ou zmodem, utilisez la fonction <guibutton>Recevoir un fichier</guibutton>.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-file.png"/>
    </para>
    <para>Quand vous êtes prêt à recevoir un fichier, cliquez simplement sur <guibutton>Recevoir un fichier</guibutton> et sélectionnez un dossier pour y enregistrer le fichier reçu. Vous devrez également sélectionner le protocole approprié en bas de la fenêtre.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-file-dialog.png"/>
    </para>
    <para>Pendant l'enregistrement, une barre de progression affiche l'état de réception du fichier.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-progress.png"/>
    </para>
    <para>moserial n'applique pas les protocoles xmodem, ymodem ou zmodem directement. Il dépend des utilitaires standards rz et sz pour envoyer et recevoir des données. Ces utilitaires font partie du paquet lrzsz, qui doit être installé sur votre système.</para>
  </sect1>

  <sect1 id="preferences">
    <title>Autres préférences</title>
    <para><application>moserial</application> possède plusieurs options qui peuvent être configurées depuis la boîte de dialogue <guilabel>Préférences</guilabel>.</para>
    <para>
      <graphic format="PNG" fileref="figures/preferences.png"/>
    </para>
    <para>Vous pouvez configurer les couleurs de premier plan et d'arrière-plan des zones de texte, la couleur de la surbrillance, ainsi que la couleur, le type et la taille de la police.</para>
    <para>Si l'option <guilabel>Lancer les fichiers enregistrés</guilabel> est activée, le fichier enregistré sera ouvert aussitôt après sa sauvegarde, en utilisant l'application par défaut pour le type du fichier. L'application par défaut est définie par l'environnement de bureau.</para>
    <para>Si l'option <guilabel>Activer le délai de connexion</guilabel> est cochée, l'enregistrement s'arrêtera automatiquement après une durée d'inactivité ajustable, si la réception de données est interrompue. C'est-à-dire que moserial attendra indéfiniment d'enregistrer les premières données avant d'activer le compteur d'inactivité.</para>
  </sect1>

  <sect1 id="profiles">
    <title>Utilisation de profils</title>
    <para><application>moserial</application> peut enregistrer ses paramètres dans un fichier de profil et les charger à partir de ce profil. Vous pouvez ainsi sauvegarder et rappeler vos configurations les plus utilisées. Pour enregistrer un profil, faites appel à la boîte de dialogue <menuchoice><guisubmenu>Fichier</guisubmenu><guimenuitem>Enregistrer la configuration</guimenuitem></menuchoice> ou <menuchoice><guisubmenu>Fichier</guisubmenu><guimenuitem>Enregistrer les paramètres sous</guimenuitem></menuchoice>. Pour charger un profil, utilisez la boîte de dialogue <menuchoice><guisubmenu>Fichier</guisubmenu><guimenuitem>Ouvrir les paramètres</guimenuitem></menuchoice>.</para>
    <para>Le chargement d'un profil déconnecte automatiquement tout port connecté.</para>
    <para>Quand vous quittez <application>moserial</application>, les paramètres en cours sont enregistrés dans un fichier de profil par défaut. Ces paramètres seront automatiquement rappelés lors du prochain lancement de <application>moserial</application>.</para>
  </sect1>

  <sect1 id="keys">
    <title>Touches spéciales</title>
    <para>
      <table id="moserial-TBL-keyword-shortcuts" frame="topbot">
      <title>Touches spéciales</title>
      <tgroup cols="2" colsep="1" rowsep="1">
        <colspec colname="COLSPEC0" colwidth="50*"/>
        <colspec colname="COLSPEC1" colwidth="50*"/>
        <thead>
          <row valign="top" rowsep="1">
            <entry colname="COLSPEC0">
                <para>Raccourci clavier</para></entry>
            <entry colname="COLSPEC1">
                <para>Description</para></entry>
          </row>
        </thead>
        <tbody>
          <row valign="top">
            <entry>
              <para>
		<keycombo><keycap>Ctrl</keycap><keycap>C</keycap></keycombo>
              </para>
            </entry>
            <entry>
              <para>Copie le texte. Peut être utilisé dans toutes les zones de texte.</para>
            </entry>
          </row>
          <row valign="top">
            <entry>
              <para>
                <keycombo><keycap>Ctrl</keycap><keycap>X</keycap></keycombo>
              </para>
            </entry>
            <entry>
              <para>Coupe le texte. Ne marche que pour la zone de texte uni-ligne <guilabel>Sortant</guilabel>.</para>
            </entry>
          </row>
          <row valign="top">
            <entry>
              <para>
                <keycombo><keycap>Ctrl</keycap><keycap>V</keycap></keycombo>
              </para>
            </entry>
            <entry>
              <para>Colle le texte dans la zone de texte uni-ligne <guilabel>Sortant</guilabel> et déplace le pointeur dans cette zone.</para>
            </entry>
          </row>
          <row valign="top">
            <entry>
              <para>
                <keycap>Échap</keycap>
              </para>
            </entry>
            <entry>
              <para>Positionne le pointeur dans la zone de texte uni-ligne <guilabel>Sortant</guilabel>.</para>
            </entry>
          </row>

        </tbody>
      </tgroup>
      </table>
  </para>
  </sect1>

</article>
