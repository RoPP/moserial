<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "3.0.0">
<!ENTITY manualversion "3.0.0">
<!ENTITY date "April 2009">
<!ENTITY appname "moserial">
<!ENTITY app "<application>moserial</application>">
<!-- replace version above with actual application version number --><!--  Template Version: 1.0.1  (do not remove this line) -->]>
<!-- =============Document Header ============================= -->
<article id="index" lang="es"> <!-- please do not change the id -->

  <articleinfo>
    <title>Manual de <application>moserial</application> V3.0.0</title>
    <copyright>
      <year>2009</year>
      <holder>Michael J. Chudobiak</holder>
    </copyright><copyright><year>2009</year><holder>Jorge González (jorgegonz@svn.gnome.org)</holder></copyright><copyright><year>2011</year><holder>Daniel Mustieles (daniel.mustieles@gmail.com)</holder></copyright>

    <!-- translators: uncomment this:

    <copyright>
     <year>2000</year>
     <holder>ME-THE-TRANSLATOR (Latin translation)</holder>
    </copyright>

    -->

    <!-- An address can be added to the publisher information.  If a role is
     not specified, the publisher/author is the same for all versions of the
     document.  -->
    <publisher>
      <publishername>Proyecto de documentación de GNOME</publishername>
    </publisher>

       <legalnotice id="legalnotice">
         <para>Se otorga permiso para copiar, distribuir y/o modificar este documento bajo los términos de la Licencia de Documentación Libre de GNU, Versión 1.1 o cualquier otra versión posterior publicada por la Free Software Foundation; sin Secciones Invariantes ni Textos de Cubierta Delantera ni Textos de Cubierta Trasera. Puede encontrar una copia de la licencia GFDL en este <ulink type="help" url="ghelp:fdl">enlace</ulink> o en el archivo COPYING-DOCS distribuido con este manual.</para>
          <para>Este manual es parte de una colección de manuales de GNOME distribuido bajo la GFDL. Si quiere distribuir este manual por separado de la colección, puede hacerlo añadiendo una copia de la licencia al manual, tal como se describe en la sección 6 de la licencia.</para>
 
         <para>Muchos de los nombres utilizados por las empresas para distinguir sus productos y servicios se consideran marcas comerciales. Cuando estos nombres aparezcan en la documentación de GNOME, y siempre que se haya informado a los miembros del Proyecto de documentación de GNOME de dichas marcas comerciales, los nombres aparecerán en mayúsculas o con las iniciales en mayúsculas.</para>
 
         <para>ESTE DOCUMENTO Y LAS VERSIONES MODIFICADAS DEL MISMO SE PROPORCIONAN SEGÚN LAS CONDICIONES ESTABLECIDAS EN LA LICENCIA DE DOCUMENTACIÓN LIBRE DE GNU (GFDL) Y TENIENDO EN CUENTA QUE: <orderedlist>
                 <listitem>
                   <para>EL DOCUMENTO SE PROPORCIONA "TAL CUAL", SIN GARANTÍA DE NINGÚN TIPO, NI EXPLÍCITA NI IMPLÍCITA INCLUYENDO, SIN LIMITACIÓN, GARANTÍA DE QUE EL DOCUMENTO O VERSIÓN MODIFICADA DE ÉSTE CAREZCA DE DEFECTOS COMERCIALES, SEA ADECUADO A UN FIN CONCRETO O INCUMPLA ALGUNA NORMATIVA. TODO EL RIESGO RELATIVO A LA CALIDAD, PRECISIÓN Y UTILIDAD DEL DOCUMENTO O SU VERSIÓN MODIFICADA RECAE EN USTED. SI CUALQUIER DOCUMENTO O VERSIÓN MODIFICADA DE AQUÉL RESULTARA DEFECTUOSO EN CUALQUIER ASPECTO, USTED (Y NO EL REDACTOR INICIAL, AUTOR O CONTRIBUYENTE) ASUMIRÁ LOS COSTES DE TODA REPARACIÓN, MANTENIMIENTO O CORRECCIÓN NECESARIOS. ESTA RENUNCIA DE GARANTÍA ES UNA PARTE ESENCIAL DE ESTA LICENCIA. NO SE AUTORIZA EL USO DE NINGÚN DOCUMENTO NI VERSIÓN MODIFICADA DE ÉSTE POR EL PRESENTE, SALVO DENTRO DEL CUMPLIMIENTO DE LA RENUNCIA;Y</para>
                 </listitem>
                 <listitem>
                   <para>EN NINGUNA CIRCUNSTANCIA NI SEGÚN NINGÚN ARGUMENTO LEGAL, SEA POR MOTIVOS CULPOSOS (INCLUIDA LA NEGLIGENCIA), CONTRACTUALES O DE OTRO TIPO, NI EL AUTOR, NI EL REDACTOR INICIAL, NI CUALQUIER COLABORADOR, NI CUALQUIER DISTRIBUIDOR DEL DOCUMENTO O VERSIÓN MODIFICADA DEL MISMO, NI CUALQUIER PROVEEDOR DE CUALQUIERA DE DICHAS PARTES, SERÁN RESPONSABLES, ANTE NINGÚN TERCERO, DE NINGÚN DAÑO O PERJUICIO DIRECTO, INDIRECTO, ESPECIAL, INCIDENTAL O CONSIGUIENTE DE NINGÚN TIPO, INCLUIDOS, SIN LIMITACIÓN, LOS DAÑOS POR PÉRDIDA DE FONDO DE COMERCIO, INTERRUPCIÓN DEL TRABAJO, FALLO O MAL FUNCIONAMIENTO INFORMÁTICO, NI CUALQUIER OTRO DAÑO O PÉRDIDA DERIVADOS DEL USO DEL DOCUMENTO Y LAS VERSIONES MODIFICADAS DEL MISMO, O RELACIONADO CON ELLO, INCLUSO SI SE HABÍA COMUNICADO A AQUELLA PARTE LA POSIBILIDAD DE TALES DAÑOS.</para>
                 </listitem>
           </orderedlist></para>
   </legalnotice>
 

    <authorgroup>
      <author role="maintainer">
        <firstname>Michael</firstname>
	<surname>Chudobiak</surname>
	<affiliation>
	  <address> <email>mjc@svn.gnome.org</email> </address>
        </affiliation>
      </author>
    </authorgroup>
    <!-- do not put authorname in the header except in copyright - use
         section "authors" below -->
    <revhistory>
      <revision>
	<revnumber>3.0.0</revnumber>
	<date>Abril de 2009</date>
	<revdescription>
	  <para role="author">Michael J. Chudobiak</para>
	  <para role="publisher">Proyecto de documentación de GNOME</para>
	</revdescription>
      </revision>
    </revhistory>


    <!-- this is version of manual, not application -->
    <releaseinfo>Este manual describe la versión 3.0.0 de moserial</releaseinfo>
    <abstract role="description"><para><application>moserial</application> es un terminal serie para el Escritorio GNOME, optimizado para inicios de sesión y captura de archivos.</para></abstract>
  </articleinfo>

  <indexterm>
    <primary>moserial</primary>
  </indexterm>
  <indexterm>
    <primary>serie</primary>
  </indexterm>
  <indexterm>
    <secondary>terminal</secondary>
  </indexterm>

  <!-- ============= Document Body ============================= -->

  <!-- ============= Introduction ============================== -->

  <sect1 id="intro">
    <title>Introducción</title>
    <para><application>moserial</application> es una terminal serie para el escritorio GNOME, optimizada el registro y la captura de archivos. <application>moserial</application> está pensado principalmente para usuarios técnicos que necesitan comunicarse con sistemas empotrados, consolas y equipos electrónicos de pruebas.</para>
    <para>A pesar de la orientación técnica de su público, <application>moserial</application> está diseñado para ser amigable. Se ha tenido cuidado de asegurar que las operaciones son sencillas de ejecutar, minimizando el número de pulsaciones necesarias. También se ha mantenido un número pequeño de opciones de configuración.</para>
    <para>Para mejorar aún mas la eficiencia, <application>moserial</application> también soporta archivos de perfil para almacenar las configuraciones usadas más habitualmente.</para>
  </sect1>

  <sect1 id="ports">
    <title>Permisos del sistema de puertos</title>
    <para>Antes de usar <application>moserial</application> con un puerto serie, debe asegurarse de que actualmente tiene permisos para leer y/o escribir en el puerto.</para>
    <para>En algunos sistemas, puede necesitar hacer algo como «sudo chmod o+rw /dev/ttyS*» y «sudo chmod o+rw /dev/ttyUSB*» para obtener permisos de lectura/escritura (para todos los usuarios) en los puertos serie. Lamentablemente, estos cambios no se guardar después de reiniciar.</para>
    <para>Alternativamente, puede ver a qué grupo de dispositivos pertenece, y hacer que su cuenta pertenezca a este grupo. En Fedora 15, por ejemplo, debe pertenecer al grupo «dialout».</para>
  </sect1>

  <sect1 id="basic-usage">
    <title>Uso básico</title>
    <sect2 id="main-toolbar">
      <title>La barra de herramientas principal</title>
      <para>A la mayoría de las funciones de <application>moserial</application> se accede a través de la barra de herramientas principal, como se muestra a continuación:</para>
      <para>
        <graphic format="PNG" fileref="figures/main-toolbar.png"/>
      </para>
    </sect2>
    <sect2 id="port-setup">
      <title>Configuración del puerto</title>
      <para>El primer paso para usar <application>moserial</application> es configurar los parámetros del puerto serie. Pulse <guibutton>Configuración del puerto</guibutton> para hacerlo. En el diálogo de configuración del puerto que usar, indique si el puerto se debe abrir para lectura, escritura o ambas, su velocidad en baudios, el número de bits de datos y de parada y el método de negociación. La configuración especificada en este diálogo invalidará cualquier configuración predeterminada del sistema para el puerto.</para>
      <para>El diálogo de configuración de puertos de <application>moserial</application> sólo los puertos /dev/ttySnn, /dev/ttyUSBnn, y /dev/rfcommnn que existen actualmente en el sistema, para valores de «nn» entre 0 y 31.</para>
      <para>Se puede activar el eco local desde este diálogo. Si está activado, todos los datos que <application>moserial</application> reciba se enviarán como eco al dispositivo emisor. Normalmente, esto debería estar desactivado, ya que puede causar un comportamiento inesperado, especialmente si el dispositivo remoto también tiene el eco activado (Esto causaría un bucle de ecos infinito.)</para>
      <para>
        <graphic format="PNG" fileref="figures/port-setup.png"/>
      </para>
    </sect2>
    <sect2 id="connect">
      <title>Abrir la conexión</title>
      <para>El siguiente paso para comunicarse con su dispositivo remoto es pulsar en <guibutton>Conectar</guibutton>. Esto abre el puerto. Si no tiene los permisos necesario para abrir el puerto, ocurrirá un error.</para>
      <para>
	<graphic format="PNG" fileref="figures/connect.png"/>
      </para>
      <para>Este paso es opcional. Si lo omite, <application>moserial</application> intentará conectar automáticamente cuando se necesite.</para>
      <para>Una vez que ha conectado, el botón <guibutton>Conectar</guibutton>. cambiará al botón <guibutton>Desconectar</guibutton>. Pulsar en <guibutton>Desconectar</guibutton>, cierra el puerto y reinicia los contadores TX y RX.</para>
      <para>
         <graphic format="PNG" fileref="figures/disconnect.png"/>
      </para>
    </sect2>
    <sect2 id="basic-io">
      <title>Entrada y salida básica</title>
      <para>Una vez que el puerto se ha abierto, cualquier texto recibido desde el dispositivo remoto se mostrará en el área de texto <guilabel>ASCII recibido</guilabel>. Si <application>moserial</application> detecta muchos caracteres no imprimibles, cambiará automáticamente a la vista <guilabel>HEX recibido</guilabel>.</para>
      <para>
        <graphic format="PNG" fileref="figures/received-tabs.png"/>
      </para>
      <para>Para cambiar entre las vistas HEX y ASCII, simplemente pulse en las pestañas apropiadas.</para>
      <para>Para enviar datos al dispositivo remoto, puede escribir texto en la caja de texto de <guilabel>Salida</guilabel> de una sola línea y pulsar el botón <guibutton>Enviar</guibutton>. Su línea de texto terminará con la secuencia seleccionada en el área de la derecha del botón <guibutton>Enviar</guibutton>. Si no está seguro qué secuencia de terminación es la mejor, empiece con «CR+LF end». El botón <guibutton>ASCII/HEX</guibutton> debería establecerse a «ASCII» para enviar texto.</para>
      <para>
        <graphic format="PNG" fileref="figures/outgoing.png"/>
      </para>
      <para>Si el botón <guibutton>ASCII/HEX</guibutton> está establecido a HEX, no introduzca texto en el área <guilabel>Salida</guilabel>. En su lugar, introduzca números hexadecimales de la forma «1234ABCD...».</para>
      <para>Una vez que sus datos se han transmitido, existirá un registro en las áreas de texto <guilabel>ASCII enviado</guilabel> y <guilabel>HEX enviado</guilabel>. Estas áreas de texto no son editables.</para>
      <para>
        <graphic format="PNG" fileref="figures/sent-tabs.png"/>
      </para>
      <para>El dispositivo remoto puede hacer eco de sus datos enviados. Si <application>moserial</application> detecta que se ha hecho eco del texto en el área de texto <guilabel>ASCII recibido</guilabel>, lo resaltará (usando el color de resaltado definido en el diálogo de preferencias).</para>
      <para>La barra de estado de la parte inferior de la ventana principal muestra el estado de un puerto y un contador de los bytes enviados (TX) y recibidos (RX). Si alguno de los datos recibidos no es imprimible, también se mostrará un contador de estos elementos.</para>
      <para>
        <graphic format="PNG" fileref="figures/statusbar.png"/>
      </para>
    </sect2>
  </sect1>
  <sect1 id="recording">
    <title>Grabar la entrada y la salida</title>
    <para><application>moserial</application> se puede usar para grabar los datos datos enviados y recibidos a un archivo. Esta función también es útil para recibir y almacenar flujos binarios de datos volcados desde un dispositivo remoto. Por ejemplo, algunos osciloscopios volcarán capturas de pantalla a un puerto serie, sin usar ningún protocolo de corrección de errores (como el Tektronix 11801).</para>
    <para>
      <graphic format="PNG" fileref="figures/record.png"/>
    </para>
    <para>Cuando esté preparado para recibir un archivo, simplemente pulse <guibutton>Grabar</guibutton>, seleccione los datos que quiere grabar (Entrada y/o Salida) en la casilla de selección <guilabel>Flujo que grabar</guilabel> e introduzca el nombre del archivo en el área del selector de archivos.</para>
    <para>
      <graphic format="PNG" fileref="figures/record-dialog.png"/>
    </para>
    <para>Cuando empieza la grabación, el botón <guibutton>Grabar</guibutton> cambia a <guibutton>Detener grabación</guibutton>. Deberá pulsarlo cuando quiera detener la grabación, o cuando haya recibido el archivo binario completo que quiera descargar.</para>
    <para>
      <graphic format="PNG" fileref="figures/stop-recording.png"/>
    </para>
    <para><application>moserial</application> puede, opcionalmente, abrir el archivo grabado con la aplicación predeterminada para ese tipo de archivo, cuando la grabación haya terminado. La grabación también puede terminar después un período de inactividad. Use el diálogo <link linkend="preferences">preferencias</link> para activar o desactivar estas características.</para>
    <para>Tenga en cuenta que si recibe archivos usando un protocolo de corrección de errores (xmodem, ymodem, o zmodem), deberá usar la función <link linkend="receiving">recibir archivo</link> en su lugar. A diferencia de la función <guibutton>Grabar</guibutton>, la función <guibutton>Recibir archivo</guibutton> puede determinar automáticamente cuando se ha terminado la descarga del archivo.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-file.png"/>
    </para>
  </sect1>
  <sect1 id="sending">
    <title>Enviar archivos</title>
    <para><application>moserial</application> puede transmitir archivos como un flujo de datos binarios, y también puede enviar archivos usando los protocolos de corrección de errores xmodem, ymodem, y zmodem. Durante la transferencia de archivos, los datos enviados no se registran en las áreas de texto <guilabel>ASCII enviado</guilabel> o <guilabel>HEX enviado</guilabel></para>
    <para>
      <graphic format="PNG" fileref="figures/send-file.png"/>
    </para>
    <para>Cuando esté listo para enviar un archivo, simplemente pulse <guibutton>Enviar archivo</guibutton> y seleccione el archivo que quiere enviar. También necesitará elegir a continuación el protocolo adecuado.</para>
    <para>
      <graphic format="PNG" fileref="figures/send-file-dialog.png"/>
    </para>
    <para>Cuando se inicia la grabación, una barra de progreso mostrará el estado de la transmisión del archivo.</para>
    <para>
      <graphic format="PNG" fileref="figures/send-progress.png"/>
    </para>
    <para>moserial no implementa los protocolos xmodem, ymodem o zmodem directamente. Se basa en las utilidades estándar rz y sz para enviar y recibir datos. Estas utilidades, que forman parte del paquete Irzsz se deben instalar en su sistema.</para>
  </sect1>

  <sect1 id="receiving">
    <title>Recibir archivos</title>
    <para><application>moserial</application> puede recibir archivos como un flujo de datos binarios, y también puede recibir archivos usando los protocolos de corrección de errores xmodem, ymodem, y zmodem. Durante la transferencia de archivos, los datos enviados no se registran en las áreas de texto <guilabel>ASCII enviado</guilabel> o <guilabel>HEX enviado</guilabel></para>
    <para>Para recibir flujos binarios de datos, use la función <link linkend="recording">grabar</link> en lugar de la función <guibutton>Recibir archivo</guibutton>.</para>
    <para>Recibir archivos enviados con los protocolos de corrección de errores xmodem, ymodem, o zmodem, usando la función <guibutton>Recibir archivo</guibutton>.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-file.png"/>
    </para>
    <para>Cuando esté preparado para recibir un archivo, simplemente pulse <guibutton>Receive File</guibutton> y seleccione una carpeta donde almacenar el archivo recibido. También necesitará seleccionar el protocolo adecuado a continuación.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-file-dialog.png"/>
    </para>
    <para>Cuando se inicia la grabación, una barra de progreso mostrará el estado de la recepción del archivo.</para>
    <para>
      <graphic format="PNG" fileref="figures/receive-progress.png"/>
    </para>
    <para>moserial no implementa los protocolos xmodem, ymodem o zmodem directamente. Se basa en las utilidades estándar rz y sz para enviar y recibir datos. Estas utilidades, que forman parte del paquete Irzsz se deben instalar en su sistema.</para>
  </sect1>

  <sect1 id="preferences">
    <title>Otras preferencias</title>
    <para><application>moserial</application> tiene varias opciones que se pueden configurar en el diálogo <guilabel>Preferencias</guilabel>.</para>
    <para>
      <graphic format="PNG" fileref="figures/preferences.png"/>
    </para>
    <para>Se puede configurar el color del área de texto de primer plano, el resaltado, y el tamaño y tipo de tipografía.</para>
    <para>Si la opción <guilabel>Lanzar los archivos guardados</guilabel> está activada, un archivo grabado se abrirá inmediatamente después de que se haya guardado, usando la aplicación predeterminada para el tipo de archivo. La aplicación predeterminada se define en el entorno de escritorio.</para>
    <para>Si la opción <guilabel>Activar el tiempo de expiración</guilabel> está activada, la grabación se detendrá automáticamente después de un período configurable de inactividad recibiendo datos. Esto significa que moserial esperará indefinidamente para grabar el primer byte de datos antes de activar el temporizador de inactividad.</para>
  </sect1>

  <sect1 id="profiles">
    <title>Usar perfiles</title>
    <para><application>moserial</application> puede cargar y guardar su configuración en archivos de perfil, por lo que puede almacenar y volver a usar las configuraciones usadas más habitualmente. Para guardar un perfil, simplemente los diálogos <menuchoice><guisubmenu>Archivo</guisubmenu><guimenuitem>Guardar</guimenuitem></menuchoice> o <menuchoice><guisubmenu>File</guisubmenu><guimenuitem>Guardar como</guimenuitem></menuchoice>. Para cargar un perfil, use el diálogo <menuchoice><guisubmenu>Archivo</guisubmenu><guimenuitem>Abrir</guimenuitem></menuchoice>.</para>
    <para>Cargar un perfil desconectará automáticamente cualquier puerto conectado.</para>
    <para>Cuando sale de <application>moserial</application>, la configuración actual se guarda en un archivo del perfil predeterminado. Esta configuración se usará automáticamente la próxima vez que ejecute <application>moserial</application>.</para>
  </sect1>

  <sect1 id="keys">
    <title>Teclas especiales</title>
    <para>
      <table id="moserial-TBL-keyword-shortcuts" frame="topbot">
      <title>Teclas especiales</title>
      <tgroup cols="2" colsep="1" rowsep="1">
        <colspec colname="COLSPEC0" colwidth="50*"/>
        <colspec colname="COLSPEC1" colwidth="50*"/>
        <thead>
          <row valign="top" rowsep="1">
            <entry colname="COLSPEC0">
                <para>Combinaciones de teclas</para></entry>
            <entry colname="COLSPEC1">
                <para>Descripción</para></entry>
          </row>
        </thead>
        <tbody>
          <row valign="top">
            <entry>
              <para>
		<keycombo><keycap>Ctrl</keycap><keycap>C</keycap></keycombo>
              </para>
            </entry>
            <entry>
              <para>Copia texto. Esto se puede usar en cualquier área de texto.</para>
            </entry>
          </row>
          <row valign="top">
            <entry>
              <para>
                <keycombo><keycap>Ctrl</keycap><keycap>X</keycap></keycombo>
              </para>
            </entry>
            <entry>
              <para>Corta texto. Esto sólo funciona en la caja de texto de <guilabel>Salida</guilabel> de una sola línea.</para>
            </entry>
          </row>
          <row valign="top">
            <entry>
              <para>
                <keycombo><keycap>Ctrl</keycap><keycap>V</keycap></keycombo>
              </para>
            </entry>
            <entry>
              <para>Pega texto en la caja de texto de <guilabel>Salida</guilabel> de una sola línea, y mueve el foco a esta caja de texto.</para>
            </entry>
          </row>
          <row valign="top">
            <entry>
              <para>
                <keycap>Esc</keycap>
              </para>
            </entry>
            <entry>
              <para>Mueve el foco a la caja de texto de <guilabel>Salida</guilabel> de una sola línea.</para>
            </entry>
          </row>

        </tbody>
      </tgroup>
      </table>
  </para>
  </sect1>

</article>
